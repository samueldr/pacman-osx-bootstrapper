Pacman Bootstrapper
===================

This set of scripts will build a clean pacman installation.

It works by, first, making a temporary prefix used to build the minimal 
dependencies, then using those minimal dependencies to build pacman. Once
pacman is built, it is finally used to make the package according to the
PKGBUILDs. Those clean packages can then be installed to the system.

The first pass builds incomplete software, in that they are the most minimally
built to make pacman and makepkg work. This means that we should skip any feature
that pulls in dependencies that are not needed. Do not build documentation either.

The script works by making use of external script for the recipes.
The built and downloaded software is cached in case something happens and a 
build fails.

Once the clean installation is made, it should be possible to add the repositories
you want, and install stuff from them. It will be important to update, since this
bootstrapping process might use older software than available.

The software is installed under /usr/local/aosuke/. This is the location chosen
for my (still in progress) distribution of software for Mac OS X. Stay tuned
for more details, they will follow once I get all this figured out a bit more.

## Known issues

These are things that are left to fix

  * Add archive checksum verification in bootstrap scripts.
  * Egg or chicken issue : how to cleanly install the package on systems without building this bootstrap?
  * The installed utilities are incomplete, only a couple of them are made.

You have to be careful while hacking on those scripts. They should run on a 
clean, untouched Mac OS X system. This means, no homebrew installation, no
pkgin installation from save-osx. This is not limited to those package 
installation tools, you should not have external utilities. The XCode CLI tools
are not considered external utilities though.

Do note that this script has to run using bash version 3, since OS X ships 
with that version.

## Credits

Some of the initial PKGBUILDs were taken or heavily inspired from https://github.com/kladd/pacman-osx-pkgs .

Part of the bootstrapping was inspired by https://github.com/kladd/pacman-osx ,
though the scripts did not work at all and were in dire need of tough love. Mainly,
what was kept was the general idea of what was needed to install.
