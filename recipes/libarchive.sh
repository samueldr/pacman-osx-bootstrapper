#!/usr/bin/env bash

build() {
	local name="libarchive"
	local version="3.1.2"
	local archive="${name}-${version}.tar.gz"

	if is_built $name; then
		return 0
	fi

	# Clean the build directory... just in case.
	_ rm -rf "$BUILDDIR/$name/${name}-${version}"
	_ mkdir -p "$BUILDDIR/$name"
	_ cd "$BUILDDIR/$name"

	if [[ ! -f "$archive" ]]; then
		_ download "http://www.libarchive.org/downloads/${archive}" "$archive"

		# Archlinux patches included.
		local patches=(
			"libarchive-3.1.2-sparce-mtree.patch"
			"libarchive-3.1.2-acl.patch"
			"0001-mtree-fix-line-filename-length-calculation.patch"
			"0001-Limit-write-requests-to-at-most-INT_MAX.patch"
		)
		for p in "${patches[@]}" ; do
			_ download "https://projects.archlinux.org/svntogit/packages.git/plain/libarchive/trunk/$p" "$p"
		done

		_ download https://github.com/libarchive/libarchive/commit/d767d7904781794442938df6b0dd29c8da325e03.patch "0002-fix-lrzip-test-case.patch"
	else
		echo "archive $archive already downloaded."
	fi

	_ tar xf "$archive"

	_ cd "${name}-${version}"

	# https://code.google.com/p/libarchive/issues/detail?id=301
	# upstream commit e65bf287f0133426b26611fe3e80b51267987106
	_ patch -Np1 -i "$BUILDDIR/$name/0001-mtree-fix-line-filename-length-calculation.patch"

	# https://code.google.com/p/libarchive/issues/detail?id=329
	_ patch -Np1 -i "$BUILDDIR/$name/libarchive-3.1.2-acl.patch"

	# CVE-2013-0211
	_ patch -Np1 -i "$BUILDDIR/$name/0001-Limit-write-requests-to-at-most-INT_MAX.patch"

	# upstream commit 977bf2a4 - improved mtree support
	_ patch -p1 -i "$BUILDDIR/$name/libarchive-3.1.2-sparce-mtree.patch"

	# upstream fix for failing lrzip test.
	_ patch -p1 -i "$BUILDDIR/$name/0002-fix-lrzip-test-case.patch"

	# Seems the previous patch does not work?
	echo > libarchive/test/test_archive_write_add_filter_by_name.c

	if [[ "$PLATFORM" == "MacOSX" ]]; then
		echo "Patching out failing tests."
		# There are issues with two tests, this basically 'NOPs' them out.
		echo > libarchive/test/test_write_disk_mac_metadata.c
		echo > libarchive/test/test_write_disk_appledouble.c
	fi

	_ ./configure --prefix="$TEMPPREFIX" --without-xml2
	_ make
	_ make check
	_ make install

	_ mark_built $name

}
