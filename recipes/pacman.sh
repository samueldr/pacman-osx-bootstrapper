#!/usr/bin/env bash

build() {
	# Seems that the source package cannot be built as-is on OSX.
	# The git package, though, can be used.


	local name="pacman"
	# name of the saved repo
	local repo="pacman-repo"
	# FIXME : Use a particular checkout instead of master...
	# Using this checkout
	local checkout="v4.2.1"

	# Unused.
	#local version="4.1.2"
	#local archive="${name}-${version}.tar.gz"

	if is_built $name; then
		return 0
	fi

	# Clean the build directory... just in case.
	# Archive
	#_ rm -rf "$BUILDDIR/$name/${name}-${version}"
	# Git checkout
	_ rm -rf "$BUILDDIR/$name/${name}"
	_ mkdir -p "$BUILDDIR/$name"
	_ cd "$BUILDDIR/$name"

	if [[ ! -d "$repo" ]]; then
		_ git clone "git://projects.archlinux.org/pacman.git" "$repo"
	else
		echo "skipping checkout... already checked out."
	fi

	_ cp -prf "$repo" "$name"
	_ cd "$name"
	_ git reset --hard
	_ git checkout "$checkout"

	local LIBARCHIVE_CFLAGS="-I$TEMPPREFIX/include"
	local LIBARCHIVE_LIBS="-L$TEMPPREFIX/lib -larchive"
	local LIBCURL_CFLAGS="-I/usr/include/curl"
	local LIBCURL_LIBS="-lcurl"

	_ ./autogen.sh
	_ ./configure --prefix=$TEMPPREFIX \
		--disable-doc \
		--with-scriptlet-shell=$TEMPPREFIX/bin/bash \
		--with-curl
	_ make
	_ make -C contrib
	_ make install
	_ make -C contrib install

	cp "$BOOTSTRAPDIR/recipes/makepkg.conf" "$TEMPPREFIX/etc/makepkg.conf"

	# Test suite seems to be b0rked with that build...
	# FIXME : Check if pacman is good enough to bootstrap itself that way
	# FIXME : Check that final-built pacman will not b0rk itself.
	#echo "Checking pacman"
	#_ make check

	_ mark_built $name

}
