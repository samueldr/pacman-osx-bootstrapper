#!/usr/bin/env bash

build() {
	local name="pkg-config"
	local version="0.28"
	local archive="${name}-${version}.tar.gz"

	if is_built $name; then
		return 0
	fi

	# Clean the build directory... just in case.
	_ rm -rf "$BUILDDIR/$name/${name}-${version}"
	_ mkdir -p "$BUILDDIR/$name"
	_ cd "$BUILDDIR/$name"

	if [[ ! -f "$archive" ]]; then
		_ download "http://${name}.freedesktop.org/releases/${archive}" "$archive"
	else
		echo "archive $archive already downloaded."
	fi
	_ tar xf "$archive"

    _ cd "${name}-${version}"

	_ ./configure --prefix=$TEMPPREFIX --with-internal-glib
	_ make
	_ make install

	_ mark_built $name

}
