#!/usr/bin/env bash

# TODO : See if we can make use of a more upstream fakeroot.
# http://anonscm.debian.org/cgit/users/clint/fakeroot.git/

build() {
	local name="fakeroot"
	local version="1.20.2"
	local archive="${name}_${version}.orig.tar.bz2"

	if is_built $name; then
		return 0
	fi

	# Clean the build directory... just in case.
	_ rm -rf "$BUILDDIR/$name/${name}-${version}"
	_ mkdir -p "$BUILDDIR/$name"
	_ cd "$BUILDDIR/$name"

	if [[ ! -f "$archive" ]]; then
		_ download "https://mirrors.kernel.org/debian/pool/main/f/fakeroot/$archive" "$archive"
		if [[ "$PLATFORM" == "MacOSX" ]]; then
			_ download "https://bugs.debian.org/cgi-bin/bugreport.cgi?msg=5;filename=0001-Implement-openat-2-wrapper-which-handles-optional-ar.patch;att=1;bug=766649" "0001-Implement-openat-2-wrapper-which-handles-optional-ar.patch"
			_ download "https://bugs.debian.org/cgi-bin/bugreport.cgi?msg=5;filename=0002-OS-X-10.10-introduced-id_t-int-in-gs-etpriority.patch;att=2;bug=766649" "0002-OS-X-10.10-introduced-id_t-int-in-gs-etpriority.patch"
		fi
	else
		echo "archive $archive already downloaded."
	fi
	_ tar xf "$archive"

	_ cd "${name}-${version}"

	# Patches from homebrew to fix compilation under Yosemite
	# https://github.com/ntalbott/homebrew/blob/972fc4f9d69b001ed0acfe870d22151924180271/Library/Formula/fakeroot.rb

	if [[ "$PLATFORM" == "MacOSX" ]]; then
		_ patch -p1 < ../0001-Implement-openat-2-wrapper-which-handles-optional-ar.patch
		_ patch -p1 < ../0002-OS-X-10.10-introduced-id_t-int-in-gs-etpriority.patch
		_ patch -p1  <<EOF
index 15fdd1d..29d738d 100644
--- a/libfakeroot.c
+++ b/libfakeroot.c
@_ -2446,6 +2446,6 @_ int openat(int dir_fd, const char *pathname, int flags, ...)
         va_end(args);
         return next_openat(dir_fd, pathname, flags, mode);
     }
-    return next_openat(dir_fd, pathname, flags);
+    return next_openat(dir_fd, pathname, flags, NULL);
 }
 #endif
EOF
	fi

	export CFLAGS="-Wno-deprecated-declarations"
	_ ./configure \
		--disable-dependency-tracking --disable-static --disable-silent-rules \
		--prefix=$TEMPPREFIX

	if [[ "$PLATFORM" == "MacOSX" ]]; then
		_ make wraptmpf.h
		_ patch <<EOF
--- wraptmpf.h.orig	2015-06-27 22:22:02.000000000 -0400
+++ wraptmpf.h	2015-06-27 22:23:20.000000000 -0400
@_ -575,6 +575,10 @@
 #endif /* HAVE_MKDIRAT */
 #ifdef HAVE_OPENAT
 extern int openat (int dir_fd, const char *pathname, int flags, ...);
+static __inline__ int next_openat (int dir_fd, const char *pathname, int flags, mode_t mode) __attribute__((always_inline));
+static __inline__ int next_openat (int dir_fd, const char *pathname, int flags, mode_t mode) {
+  return openat (dir_fd, pathname, flags, mode);
+}
 
 #endif /* HAVE_OPENAT */
 #ifdef HAVE_RENAMEAT
EOF
	fi
	_ make
	_ make install

	_ mark_built $name

}
