#!/usr/bin/env bash

# Harden the bash script
set -e # Bails-out on first error
set -u # Errors on unbound variables

###############################################################################
# Configuration variables                                                     #
###############################################################################

# The folder used for the temporary build
# It uses the cwd by default.
export TEMPPREFIX="$PWD/tempprefix"
# Transactional directory
export BUILDDIR="$PWD/build/"

# This is the prefix that will loosely follow the FHS for our packages.
# No trailing slashes.
export FINALPREFIX="/usr/local/aosuke"

# FIXME : Properly detect this folder.
export BOOTSTRAPDIR="$PWD/"

PLATFORM="Unknown"
if [[ "$(uname -s)" == "Linux" ]]; then
	PLATFORM="Linux"
fi
if [[ "$(uname -s)" == "Darwin" ]]; then
	PLATFORM="MacOSX"
fi

# Temporary package build order
export TMPPKGS=(
	"gettext"
	"autoconf"
	"automake"
	"pkg-config"
	"libtool"
	"bash"
	"libarchive"
	"fakeroot"
	"pacman"
)

# PKGBUILD build order
export PACKAGES=()

if [[ "$PLATFORM" == "Linux" ]]; then
	PACKAGES+=("linux-lsb")
fi
if [[ "$PLATFORM" == "MacOSX" ]]; then
	PACKAGES+=("osx")
fi
PACKAGES+=("gettext")
PACKAGES+=("readline")
PACKAGES+=("bash")
PACKAGES+=("fakeroot")
PACKAGES+=("xz")
PACKAGES+=("libarchive")
PACKAGES+=("pacman")

# Version... internal use mainly...
export VERSION="0.2"

###############################################################################
# Utility functions                                                           #
###############################################################################

_() {
	echo '>' "$@"
	"$@"
}

download() {
	local url="$1"
	local out="$2"
	curl -L -o "$out" "$url"
}

is_built() {
	if [[ -f "$TEMPPREFIX/built/$1" ]]; then
		echo "$1 already built... skipping"
		return 0
	fi
	return 1
}

mark_built() {
	touch "$TEMPPREFIX/built/$1"
}

aosukepacman() {
	# Shim to call the temporary pacman to install to the final system
	# FIXME : Use an alternate config file instead?
	_ sudo "$TEMPPREFIX/bin/pacman" \
		--dbpath "$FINALPREFIX/var/lib/pacman/" \
		--cachedir "$FINALPREFIX/var/cache/pacman/pkg" \
		--logfile "$FINALPREFIX/var/log/pacman.log" \
		"$@"
}
tempmakepkg() {
	"$TEMPPREFIX/bin/makepkg" -d "$@"
}

###############################################################################
# Main script                                                                 #
###############################################################################

export ORIGPATH="$PATH"
export     PATH="$TEMPPREFIX/bin/:$PATH"

bootstrap() {
	# Some preparation
	echo ":: Bootstrapping temporary build."
	echo "Making directories"
	_ mkdir -p $BUILDDIR
	_ mkdir -p $TEMPPREFIX
	_ mkdir -p $TEMPPREFIX/built/

	export  CFLAGS="-I$TEMPPREFIX/include"
	export LDFLAGS="-L$TEMPPREFIX/lib"

	local cwd=$PWD

	for package in "${TMPPKGS[@]}"; do
		cd $cwd
		echo ":: Building $package ..."
		. "recipes/${package}.sh"
		build "$@"
		echo
	done

	unset CFLAGS
	unset LDFLAGS

	echo ":: Done bootstrapping!"
}

# Here we prepare the system to receive our PREFIX
preparesystem() {
	_ sudo mkdir -p "$FINALPREFIX"
	_ sudo chmod 755 "$FINALPREFIX"
	_ sudo chown 0:0 "$FINALPREFIX"
	_ sudo mkdir -p "$FINALPREFIX/var/lib/pacman"
	_ sudo mkdir -p "$FINALPREFIX/var/log"
}

# Here we build and install the packages from the PKGBUILD
makepackages() {
	export     PATH="$FINALPREFIX/bin/:$PATH"
	export BUILDDIR="$BOOTSTRAPDIR/tmpmakepkg"
	export   PACMAN="$TEMPPREFIX/bin/pacman"
	local built

	echo ":: Building final packages."
	_ mkdir -p "$BUILDDIR"
	for package in "${PACKAGES[@]}"; do
		echo ":: Building $package ..."
		pushd "$BOOTSTRAPDIR/PKGBUILDs/$package/"

		# If the build fails...
		if ! tempmakepkg; then
			built=$(LANG=C tempmakepkg 2>&1 | head -n 1)
			echo ":: $built"
			if [[ "$built" != "==> ERROR: A package has already been built. (use -f to overwrite)" ]]; then
				echo "Failure to build package."
				return 1
			fi
		fi
		aosukepacman --noconfirm -U ./*.pkg.tar*
		popd
		echo
	done

	echo ":: Done building final packages."
}

main() {
	echo ":: Pacman on OSX bootstrapping script"
	echo "   script v${VERSION}"
	echo

	bootstrap "$@"
	preparesystem "$@"
	makepackages "$@"
}

time main "$@"
